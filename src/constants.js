export const WORK_FILTERS = {
  ALL: "all",
  BY_CLIENT: "byclient",
  BY_TECH: "bytech"
};
