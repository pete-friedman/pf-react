
<ul id="scene" className="scene border fill">
    <li className="layer expand-width" data-depth="0.1">
        <div className="sky1">
            <img src="./img/galaxy-sm.jpg" alt="" className="galaxy-bg" />
        </div>
    </li>
    <li className="layer expand-width" data-depth="0.2" style={{
        opacity: .6
    }}>
        <div className="planet-container">
            <img src={planetSvg} alt="planet" className="planet9" />
            <img src={ringsSvg} alt="planet9" className="planet-rings" />


        </div>

    </li>
    <li className="layer expand-width" data-depth="0.26" style={{
        zIndex: 2
    }}>
        <div className="bg1">
            <img src="./img/ls-bg-sm.png" alt="" className="ls-img" />
        </div>
    </li>

    <li className="layer expand-width" data-depth="0.65" style={{
        zIndex: 3
    }}>
        <div className="fg3">
            <img src="./img/dejeuner.png" alt="" className="dejeuner" />
        </div>
    </li>
    <li className="layer expand-width" data-depth="0.76" style={{
        zIndex: 4
    }}>
        <div className="fg1">
            <img alt="" src="./img/ls-foreground1.png" />
        </div>
    </li>
</ul>



<div id="bioImageContainer">
    <div className="abs bio-image zero-opacity" id="frame1">
        <img src={process.env.PUBLIC_URL + "/img/pete/frame1.png"} alt="" className="r-img" />
    </div>
    <div className="abs bio-image zero-opacity" id="frame2">
        <img src={process.env.PUBLIC_URL + "/img/pete/frame2.png"} alt="" className="r-img" />
    </div>
    <div className="abs bio-image zero-opacity" id="frame3">
        <img src={process.env.PUBLIC_URL + "/img/pete/frame3.png"} alt="" className="r-img" />
    </div>
    <div className="abs bio-image zero-opacity" id="frame4">
        <img src={process.env.PUBLIC_URL + "/img/pete/frame4.png"} alt="" className="r-img" />
    </div>
    <div className="abs bio-image zero-opacity" id="frame5">
        <img src={process.env.PUBLIC_URL + "/img/pete/frame5.png"} alt="" className="r-img" />
    </div>
    <div className="abs bio-image zero-opacity" id="frame6">
        <img src={process.env.PUBLIC_URL + "/img/pete/frame6.png"} alt="" className="r-img" />
    </div>
    <div className="abs bio-image zero-opacity" id="frame7">
        <img src={process.env.PUBLIC_URL + "/img/pete/frame7.png"} alt="" className="r-img" />
    </div>
    <div className="abs bio-image zero-opacity" id="frame8">
        <img src={process.env.PUBLIC_URL + "/img/pete/frame8.png"} alt="" className="r-img" />
    </div>
    <div className="abs bio-image zero-opacity" id="frame9">
        <img src={process.env.PUBLIC_URL + "/img/pete/frame9.png"} alt="" className="r-img" />
    </div>
</div>



    <div className="font-main medium-text off-white">Pete Friedman is a Seattle area developer with interests in photography, web engineering, and immersive multimedia experiences. Pete likes to explore the dynamic intersection of creative design, meaningful storytelling and the myriad of tools for building applications on the web.
                <br /><br />
                Currently, Pete is developing projects that leverage the scale and security of tools offered by PAAS vendors like AWS, Netlify and MongoDB Atlas. On the front end, Pete likes the composition based approach of components with modern frameworks like Vue and React.
                <br /><br />
    </div>
            




<Row noGutters={true}>
    <Col xs={12}>
        <div className="content-holder">
            <div className="content-text-block d-flex justify-content-center">
                <div className="font-main medium-text off-white">
                    Previously, Pete directed the digital studio at <a target="_blank" rel="noopener noreferrer" href="//www.publicisseattle.com">Publicis Seattle</a>,
            contributed to authoring the video framework API at <a target="_blank" rel="noopener noreferrer" href="//www.disney.com">The Walt Disney Company</a>, and worked on CMS design integration at <a target="_blank" rel="noopener noreferrer" href="//www.cdkglobal.com">The Cobalt Group</a>.
            <br /><br /><br />
                </div>
            </div>
        </div>
    </Col>
</Row>

<Row className="no-gutters pad">
    <Col xs={12}>
        <div >
            <div className="d-flex justify-content-center">

                <div id="bioImageContainer">
                    <div className="abs bio-image zero-opacity" id="frame1">
                        <img src={process.env.PUBLIC_URL + "/img/pete/frame1.png"} alt="" className="r-img" />
                    </div>
                    <div className="abs bio-image zero-opacity" id="frame2">
                        <img src={process.env.PUBLIC_URL + "/img/pete/frame2.png"} alt="" className="r-img" />
                    </div>
                    <div className="abs bio-image zero-opacity" id="frame3">
                        <img src={process.env.PUBLIC_URL + "/img/pete/frame3.png"} alt="" className="r-img" />
                    </div>
                    <div className="abs bio-image zero-opacity" id="frame4">
                        <img src={process.env.PUBLIC_URL + "/img/pete/frame4.png"} alt="" className="r-img" />
                    </div>
                    <div className="abs bio-image zero-opacity" id="frame5">
                        <img src={process.env.PUBLIC_URL + "/img/pete/frame5.png"} alt="" className="r-img" />
                    </div>
                    <div className="abs bio-image zero-opacity" id="frame6">
                        <img src={process.env.PUBLIC_URL + "/img/pete/frame6.png"} alt="" className="r-img" />
                    </div>
                    <div className="abs bio-image zero-opacity" id="frame7">
                        <img src={process.env.PUBLIC_URL + "/img/pete/frame7.png"} alt="" className="r-img" />
                    </div>
                    <div className="abs bio-image zero-opacity" id="frame8">
                        <img src={process.env.PUBLIC_URL + "/img/pete/frame8.png"} alt="" className="r-img" />
                    </div>
                    <div className="abs bio-image zero-opacity" id="frame9">
                        <img src={process.env.PUBLIC_URL + "/img/pete/frame9.png"} alt="" className="r-img" />
                    </div>
                </div>
                <Waypoint onEnter={this.beginTransport} onLeave={this.endTransport} />
            </div>
        </div>
    </Col>
</Row>