import React from 'react';
import ReactDOM from 'react-dom';
import ReactGA from 'react-ga';
import App from './App.jsx';
import registerServiceWorker from './registerServiceWorker';
ReactGA.initialize('UA-145270540-1');
ReactDOM.render( < App />,document.getElementById('root'));
registerServiceWorker();