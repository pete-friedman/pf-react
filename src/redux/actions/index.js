import {WORK_SET_FILTER, WORK_SET_SUBFILTER} from "./actionTypes";

export const workSetFilter = workType => ({
  type: WORK_SET_FILTER,
  payload: { workType }
});

export const workSetSubFilter = workItem => ({
  type: WORK_SET_SUBFILTER,
  payload: { workItem }
});
