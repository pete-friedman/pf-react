import {WORK_SET_FILTER, WORK_SET_SUBFILTER} from "../actions/actionTypes";

const initialState = {
  filterBy: "clear",
  filterItem: "clear"
};

const workReducer = (state = initialState, action) => {
  console.log("workReducer:"+ JSON.stringify(action))
  switch (action.type) {
    case WORK_SET_FILTER: {
      console.log("redux workReducer set_filter");
      const { filterBy } = action.payload;
      return {
        ...state,filterBy: {...state.filterBy}
      };
    }
    case WORK_SET_SUBFILTER: {
      console.log("redux workReducer set_sub_filter");
      const { filterItem } = action.payload;
      return {
        ...state,
        filterBy: {...state.filterBy},
        filterItem: {
          ...state.filterItem,
        }
      };
    }
    default:
      console.log("work reducer default return state");
      return state;
  }
}
export default workReducer
