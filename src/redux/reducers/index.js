import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';
import workReducer from './workReducer';

const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  workReducer
})

export default createRootReducer
